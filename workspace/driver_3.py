'''
edX AI Project 5
'''
import glob
import os
import pandas
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier

TRAIN_PATH = "../resource/asnlib/public/aclImdb/train/"
TEST_PATH = "../resource/asnlib/public/imdb_te.csv"
STOP_WORDS = [line.replace('\n', '') for line in open('stopwords.en.txt', 'r')]

def process_files(start_id, path, sentiment):
    '''
    Read all IMDB reviews from a file directory
    '''
    data = []
    for filename in glob.glob(os.path.join(path, '*.txt')):
        file = open(filename, 'r', encoding='utf8')
        review = str(file.read()).replace('"', '""')
        file.close()
        data.append([start_id, review, sentiment])
        start_id += 1
    return data

def imdb_data_preprocess(inpath, name="imdb_tr.csv"):
    '''
    Implement this module to extract
    and combine text files under train_path directory into
    imdb_tr.csv. Each text file in train_path should be stored
    as a row in imdb_tr.csv. And imdb_tr.csv should have two
    columns, "text" and label
    '''
    data = [['', 'text', 'polarity']]
    data += process_files(0, os.path.join(inpath, 'pos/'), 1)
    data += process_files(len(data), os.path.join(inpath, 'neg/'), 0)
    file = open(name, 'w', encoding='utf8')
    for item in data:
        file.write(str(item[0]) + ',"' + item[1] + '",' + str(item[2]) + '\n')
    file.close()

def ngram_prediction(dataframe, output_file, ngram=1, tfidf=False):
    '''
    train a SGD classifier using n-gram representation,
    predict sentiments on imdb_te.csv, and write output to
    output_file
    '''
    if tfidf:
        vectorizer = TfidfVectorizer(ngram_range=(1, ngram), stop_words=STOP_WORDS)
    else:
        vectorizer = CountVectorizer(ngram_range=(1, ngram), stop_words=STOP_WORDS)

    x_train = vectorizer.fit_transform(dataframe['text'])
    y_train = dataframe['polarity']

    clf = SGDClassifier(loss="hinge", penalty='l1')
    clf.fit(x_train, y_train)

    test_df = pandas.read_csv(TEST_PATH, encoding='ISO-8859-1')
    x_test = vectorizer.transform(test_df['text'])

    predictions = clf.predict(x_test)
    file = open(output_file, 'w')
    for pred in predictions:
        file.write(str(pred) + '\n')
    file.close()

def unigram(dataframe):
    '''
    train a SGD classifier using unigram representation,
    predict sentiments on imdb_te.csv, and write output to
    unigram.output.txt
    '''
    ngram_prediction(dataframe, 'unigram.output.txt')

def bigram(dataframe):
    '''
    train a SGD classifier using bigram representation,
    predict sentiments on imdb_te.csv, and write output to
    bigram.output.txt
    '''
    ngram_prediction(dataframe, 'bigram.output.txt', 2)

def unigram_tfidf(dataframe):
    '''
    train a SGD classifier using unigram representation
    with tf-idf, predict sentiments on imdb_te.csv, and write
    output to unigramtfidf.output.txt
    '''
    ngram_prediction(dataframe, 'unigramtfidf.output.txt', 1, True)

def bigram_tfidf(dataframe):
    '''
    train a SGD classifier using bigram representation
    with tf-idf, predict sentiments on imdb_te.csv, and write
    output to bigramtfidf.output.txt
    '''
    ngram_prediction(dataframe, 'bigramtfidf.output.txt', 2, True)

if __name__ == "__main__":
    imdb_data_preprocess(TRAIN_PATH)
    DF = pandas.read_csv('imdb_tr.csv', encoding='ISO-8859-1')
    unigram(DF)
    bigram(DF)
    unigram_tfidf(DF)
    bigram_tfidf(DF)
